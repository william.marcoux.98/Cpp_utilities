/**
* @file RingBuffer.tcc
* @brief Definition file the RingBuffer class.
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 14th, 2017
*
* The RingBuffer class comprises a circular buffer with management methods.
*
*/

#include "RingBuffer.hpp"

template<class T>
RingBuffer<T>::RingBuffer()
{
	m_maxSize = DEFAULT_MAX_SIZE;
	m_head = 0;
	m_tail = 0;
	m_size = 0;
	m_buffer = new T[DEFAULT_MAX_SIZE];
}

//template RingBuffer<char>::RingBuffer();

template<class T>
RingBuffer<T>::RingBuffer(size_t maxSize)
{
	m_maxSize = maxSize;
	m_head = 0;
	m_tail = 0;
	m_size = 0;
	m_buffer = new T[maxSize];
}

template<class T>
RingBuffer<T>::~RingBuffer()
{
	free(m_buffer);
}

template<class T>
void RingBuffer<T>::reset()
{
	m_head = 0;
	m_tail = 0;
	m_size = 0;
}

template<class T>
void RingBuffer<T>::appendData(T* newData, size_t size)
{
	bool passedTail = false;
	
	for(size_t i = 0; i < size; ++i)
	{
		m_buffer[m_head]  = newData[i];
		m_head = (m_head + 1) % m_maxSize;
		
		if(m_head == m_tail)
		passedTail = true;
		
		if(!passedTail)
		++m_size;
	}
	
	if(passedTail)
	m_tail = (m_head + 1) % m_maxSize;
}

template<class T>
void RingBuffer<T>::prependData(T* newData, size_t size)
{
	bool passedHead = false;
	
	for(size_t i = (size - 1); i != -1; --i)
	{
		m_tail = (m_tail == 0) ? (m_maxSize - 1) : --m_tail;
		m_buffer[m_tail] = newData[i];
		
		if(m_tail == m_head)
		passedHead = true;
		
		if(!passedHead && i)
		++m_size;
	}
	
	if(passedHead)
	m_head = (m_tail == 0) ? (m_maxSize - 1) : --m_tail;
}

template<class T>
void RingBuffer<T>::insertData(T* newData, size_t size, size_t offset)
{
	size_t tmpBufferSize = (m_head < (m_tail + offset)) ? (m_maxSize - (m_tail + offset)) + m_head : m_head - (m_tail + offset);
	T tmpBuffer[tmpBufferSize];
	
	if((m_tail + offset + tmpBufferSize) % m_maxSize < (m_tail + offset) % m_maxSize)
	{
		memcpy(tmpBuffer, &m_buffer[(m_tail+offset)], (m_maxSize - (m_tail + offset)));
		memcpy(&tmpBuffer[(m_maxSize - (m_tail + offset))], &m_buffer[0], m_head);
	}
	else
	{
		memcpy(tmpBuffer, &m_buffer[(m_tail+offset) % m_maxSize], tmpBufferSize);
	}

	m_head = (m_tail + offset) % m_maxSize;
	
	for(size_t i = 0; i < size; ++i)
	{
		m_buffer[m_head]  = newData[i];
		m_head = (m_head + 1) % m_maxSize;
		
		if(((m_head + 1) % m_maxSize) == m_tail)
		return;
	}
	
	for(size_t i = 0; i < tmpBufferSize; ++i)
	{
		m_buffer[m_head]  = tmpBuffer[i];
		m_head = (m_head + 1) % m_maxSize;
		
		if(((m_head + 1) % m_maxSize) == m_tail)
		return;
	}
	
	m_size += size;
}

template<class T>
void RingBuffer<T>::getData(T* outData, size_t size)
{
	for(size_t i = 0; i < size; ++i)
	{
		outData[i] = m_buffer[(m_tail + i) % m_maxSize];
		
		if((m_tail + i) % m_maxSize == m_head)
		return;
	}
}

template<class T>
void RingBuffer<T>::getData(T* outData, size_t size, size_t offset)
{
	for(size_t i = offset; i < (size + offset); ++i)
	{
		outData[i - offset] = m_buffer[(m_tail + i) % m_maxSize];
		
		if((m_tail + i) % m_maxSize == m_head)
		return;
	}
}

template<class T>
void RingBuffer<T>::lopData(size_t size)
{
	m_tail = (size > m_size ) ? m_head : (m_tail + size) % m_maxSize;
	m_size = (size > m_size ) ? 0 : m_size - size;
}

template<class T>
void RingBuffer<T>::popData(size_t size)
{
	if(size > m_size)
	m_head = m_tail;
	else
	m_head = (m_tail + (m_size - size)) % m_maxSize;

	m_size = (size >= m_size ) ? 0 : m_size - size;
}

template<class T>
void RingBuffer<T>::freeData(size_t size, size_t offset)
{
	if(size > m_size)
	{
		reset();
		return;
	}
	
	for(size_t i = 0; i < (m_size  - (size + offset)); ++i)
	m_buffer[(m_tail + i + offset) % m_maxSize] = m_buffer[(m_tail + i + size + offset) % m_maxSize];
	
	m_size -= size;
	m_head = (m_tail + m_size) % m_maxSize;
	
}

template<class T>
void RingBuffer<T>::setHead(size_t head)
{
	if(head > m_maxSize)
	return;
	
	m_head = head;
}

template<class T>
size_t RingBuffer<T>::getHead(void) const
{
	return m_head;
}

template<class T>
void RingBuffer<T>::setTail(size_t tail)
{
	if(tail > m_maxSize)
	return;
	
	m_tail = tail;
}

template<class T>
size_t RingBuffer<T>::getTail(void) const
{
	return m_tail;
}

template<class T>
void RingBuffer<T>::setSize(size_t size)
{
	if(size > m_maxSize)
	return;
	
	m_size = size;
}

template<class T>
size_t RingBuffer<T>::getSize(void) const
{
	return m_size;
}

template<class T>
void RingBuffer<T>::setMaxSize(size_t size)
{
	if(size > MAX_SAFE_SIZE)
		return;
	
	m_maxSize = size;
	
	free(m_buffer);
	m_buffer = new T[size];
}

template<class T>
size_t RingBuffer<T>::getMaxSize(void) const
{
	return m_maxSize;
}

template<class T>
size_t RingBuffer<T>::getIndexOf(T* needle, size_t size, size_t offset)
{
	size_t foundLead = 0;
	size_t j = 0;
	
	for(size_t i = offset; i < m_size; ++i)
	{
		if(needle[j] == m_buffer[(m_tail + i) % m_maxSize])
		{
			if(!j)
			foundLead = i;
			
			if(j == (size - 1))
			return foundLead;
			
			++j;
		}
		else
		{
			j = 0;
		}
	}
	return 0;
}

template<class T>
size_t RingBuffer<T>::getIndexOf(T* needle, size_t size)
{
	return getIndexOf(needle, size, 0);
}

template<class T>
bool RingBuffer<T>::contains(T* needle, size_t size)
{
	if(getIndexOf(needle, size, 0) == 0 && m_buffer[m_tail] != needle[0])
		return false;
	else
		return true;
}

template<class T>
bool RingBuffer<T>::isEmpty(void)
{
	return (!m_size);
}

template<class T>
bool RingBuffer<T>::isFull(void)
{
	return (m_size == m_maxSize);
}