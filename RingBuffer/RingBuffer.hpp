/**
* @file RingBuffer.h
* @brief Header file the RingBuffer class.
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date February 14th, 2017
*
* The RingBuffer class comprises a circular buffer with management methods.
*
*/

#ifndef RINGBUFFER_H_
#define RINGBUFFER_H_

#define DEFAULT_MAX_SIZE 128
#define MAX_SAFE_SIZE 256

#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "funcsLib.h"

template<class T>
class RingBuffer
{
	private:
	size_t m_maxSize;
	size_t m_head;
	size_t m_tail;
	size_t m_size;
	T* m_buffer;
	
	public:
	RingBuffer();
	RingBuffer(size_t maxSize);
	~RingBuffer();
	
	void reset();
	
	void appendData(T* newData, size_t size);
	void prependData(T* newData, size_t size);
	void insertData(T* outData, size_t size, size_t offset);
	
	void getData(T* outData, size_t size);
	void getData(T* outData, size_t offset, size_t size);
	
	void lopData(size_t size);
	void popData(size_t size);
	void freeData(size_t size, size_t offset);
	
	void setHead(size_t head);
	size_t getHead(void) const;
	
	void setTail(size_t tail);
	size_t getTail(void) const;
	
	void setSize(size_t size);
	size_t getSize(void) const;
	
	void setMaxSize(size_t size);
	size_t getMaxSize(void) const;
	
	size_t getIndexOf(T* needle, size_t size, size_t offset);
	size_t getIndexOf(T* needle, size_t size);
	
	bool contains(T* needle, size_t size);
	
	bool isEmpty(void);
	bool isFull(void);
};

#include "RingBuffer.tcc"

#endif /* RINGBUFFER_H_ */