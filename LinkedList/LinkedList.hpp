/**
* @file linkedlist.h
* @brief Header file containing a linked list class and its node
* @author William Marcoux (mailto:william.marcoux.98@gmail.com)
* @version 1.0.0
* @date May 20th, 2017
*
* This file is meant to be used as a standard linked list
* for practically anything. It was made in order to
* avoid ever having to program another linked list.
*
*/

#pragma once

//#ifndef LINKEDLIST_H
//#define LINKEDLIST_H

#include <list>

template <typename T>
/**
* @class Node
* @brief Node model for the linked list
*/
class Node
{
public:
	/**
	* @fn Node ()
	* @brief Node's constructor
	*/
	Node()
	{
		previous = 0;
		next = 0;
	}

	/**
	* @fn Node (const T &dataTmp)
	* @brief Node's constructor by parameter
	*/
	Node(const T &dataTmp)
	{
		next = 0;
		previous = 0;
		data = dataTmp;
	}

	/**
	* @fn ~Node ()
	* @brief Node's destructor
	*/
	~Node()
	{

	}

	/**
	* @brief Data held by the Node
	*/
	T data;

	/**
	* @brief Pointer to the next Node
	*/
	Node *next;

	/**
	* @brief Pointer to the previous Node
	*/
	Node *previous;
};

template <typename T>

/**
* @class LinkedList
* @brief Class that does most of the operations concerning the linked list
*/
class LinkedList
{
private:
	/**
	* @brief Size of the linked list described by the number of Nodes it contains
	*/
	unsigned int _size;

	/**
	* @brief Pointer to the first Node contained in the list
	*/
	Node<T> *_head;

	/**
	* @brief Pointer to the last Node contained in the list
	*/
	Node<T> *_tail;

	/**
	* @brief Repeatedly used operation in the other methods that was simplified to a single private method
	* @param index Request for the position of the Node that is being returned
	* @return The Node corresponding to the index requested
	*/
	Node<T>* getNodeAt(const unsigned int& index = 0) const
	{
		if (index > (_size - 1))
			return _tail;
		if (index < _size / 2)
		{
			Node<T> *current = _head;
			for (unsigned int i = 0; i < index; ++i)
				current = current->next;
			return current;
		}
		else
		{
			Node<T> *current = _tail;
			for (unsigned int i = (_size - 1); i > index; --i)
				current = current->previous;
			return current;
		}
		return nullptr;
	}
	
public:

	/**
	* @brief Default constructor of the LinkedList class
	*/
	LinkedList()
	{
		_size = 0;
		_head = 0;
		_tail = 0;
	}

	/**
	* @brief Constructor by copy of the LinkedList class
	*/
	LinkedList(const LinkedList &linkedList)
	{
		*this = linkedList;
	}

	/**
	* @brief Adds a new Node at the end of the linked list
	* @param data Data held by the newly added Node
	*/
	void append(const T& data)
	{
		Node<T> *tmp = new Node<T>(data);
		if (!_size)
		{
			_head = tmp;
			_tail = tmp;
		}
		else
		{
			tmp->previous = _tail;
			_tail->next = tmp;
			_tail = tmp;
		}
		_size++;
	}

	/**
	* @brief Adds a new Node at the beginning of the linked list
	* @param data Data held by the newly added Node
	*/
	void prepend(const T &data)
	{
		Node<T> *tmp = new Node<T>(data);
		if (!_size)
		{
			_head = tmp;
			_tail = tmp;
		}
		else
		{
			tmp->next = _head;
			_head->previous = tmp;
			_head = tmp;
		}
		_size++;
	}

	/**
	* @brief Inserts a new Node at a certain position in the linked list
	* @param data Data held by the newly added Node
	* @param index Position of the newly added Node
	*/
	void insert(const T &data, const unsigned int &index = 0)
	{
		Node<T> *tmp = new Node<T>(data);
		if (index > (_size - 1))
			append(data);
		else if (!index)
			prepend(data);
		else
		{
			if (index < (_size / 2))
			{
				tmp->next = _head;
				for (unsigned int i = 0; i < index; ++i)	//store according to nexts
					tmp->next = (tmp->next)->next;
				tmp->previous = (tmp->next)->previous;
			}
			else
			{
				tmp->previous = _tail;
				for (unsigned int i = _size; i > index; --i) //store according to previous
					tmp->previous = (tmp->previous)->previous;
				tmp->next = (tmp->previous)->next;
			}
			(tmp->previous)->next = tmp;
			(tmp->next)->previous = tmp;
			_size++;
		}
	}

	/**
	* @brief Setter of the data held by the Node at the position described by the index
	* @param data New data to set in the requested Node
	* @param index Position of the requested Node
	*/
	void setAt(const T &data, const unsigned int &index = 0)
	{
		getNodeAt(index)->data = data;
	}

	/**
	* @brief Removes a Node from the list
	* @param index Position of the Node to remove
	*/
	void dropAt(const unsigned int &index = 0)
	{
		Node<T> *current = getNodeAt(index);
		if (!current->next)
		{
			(current->previous)->next = 0;
			_tail = current->previous;
		}
		else if (current != _head)
			(current->previous)->next = current->next;
		if (!current->previous)
		{
			(current->next)->previous = 0;
			_head = current->next;
		}
		else if (current != _tail)
			(current->next)->previous = current->previous;
		delete current;
		_size--;
		if (!_size)
		{
			_head = 0;
			_tail = 0;
		}
	}

	/**
	* @brief Removes all the nodes containing the data specified
	* @param data Data corresponding to the Nodes that have to be removed
	*/
	void drop(const T &data)
	{
		for (unsigned int i = 0; i < _size; ++i)
		{
			while (getNodeAt(0)->data == data)
				dropAt(0);
			if (getNodeAt(i)->data == data)
			{
				dropAt(i);
				i -= 2;
			}
		}
	}

	/**
	* @brief Removes a set of Nodes from the linked list, doesn't work normally if index_1 is greater than index_2
	* @param index_1 Position of the first Node to remove
	* @param index_2 Position of the second Node to remove
	*/
	void dropList(const unsigned int &index_1 = 0, const unsigned int &index_2 = 0)
	{
		Node<T>* delHead;
		if (index_1 > index_2)
			delHead = getNodeAt(index_2);
		else
			delHead = getNodeAt(index_1);
		Node<T>* delTail = getNodeAt(index_2);
		if (delHead == _head && delTail == _tail)
		{
			clear();
			delHead = delTail;
		}
		else if (delHead == _head)
		{
			(delTail->next)->previous = 0;
			_head = delTail->next;
		}
		else if (delTail == _tail)
		{
			(delHead->previous)->next = 0;
			_tail = delHead->previous;
		}
		else
		{
			(delHead->previous)->next = delTail->next;
			(delTail->next)->previous = delHead->previous;
		}

		while (delHead != delTail)
		{
			delHead = delHead->next;
			delete (delHead->previous);
		}
		delete delTail;
		_size -= (index_2 - index_1) + 1;
	}

	/**
	* @brief Removes the last Node from the linked list
	*/
	void dropLast()
	{
		dropAt(_size - 1);
	}

	/**
	* @brief Removes the first Node from the linked list
	*/
	void dropFirst()
	{
		dropAt(0);
	}

	/**
	* @brief Combines two linked lists by adding the one in the parameters at the end of the main one
	* @param linkedList Linked list to add to the main linked list
	*/
	void appendList(const LinkedList &linkedList)
	{
		for (unsigned int i = 0; i < linkedList.size(); ++i)
			append(linkedList.cat(i));
	}

	/**
	* @brief Combines two linked lists by adding the one in the parameters at the beginning of the main one
	* @param linkedList Linked list to add to the main linked list
	*/
	void prependList(const LinkedList &linkedList)
	{
		for (unsigned int i = linkedList.size(); i > 0; --i)
			prepend(linkedList.cat(i - 1));
	}

	/**
	* @brief Inserts the linked list in the parameters inside the main linked list
	* @param linkedList Linked List to add to the main linked list 
	* @param index Position to which insert the linked list in parameters 
	*/
	void insertList(const LinkedList &linkedList, const unsigned int &index = 0)
	{
		if (index > (_size - 1))
			appendList(linkedList);
		for (unsigned int i = 0; i < linkedList.size(); ++i)
			insert(linkedList.cat(i), (index + i));
	}

	/**
	* @brief Clears the linked list by deleting all Nodes
	*/
	void clear()
	{
		Node<T> *current = _head;
		while (_size)
		{
			current = _head->next;
			delete _head;
			_size--;
			_head = current;
		}
	}

	/**
	* @brief Swaps the position of two Nodes in the linked list
	* @param index_1 Postion of the first Node to swap
	* @param index_2 Position of the second Node to swap
	*/
	void swap(const unsigned int &index_1 = 0, const unsigned int &index_2 = 0)
	{
		T tmpData = getNodeAt(index_1)->data;
		setAt(getNodeAt(index_2)->data, index_1);
		setAt(tmpData, index_2);
	}

	/**
	* @brief Getter of the data held in the Node at the position requested by the index parameter
	* @param index Position of the Node requested
	* @return The data held in the Node at the position requested by the index parameter
	*/
	T &at(const unsigned int &index = 0)
	{
		return (getNodeAt(index)->data);
	}

	/**
	* @brief Constant getter of the data held in the Node at the position requested by the index parameter
	* @param index Position of the Node requested
	* @return The data held in the Node at the position requested by the index parameter
	*/
	T &cat(const unsigned int &index = 0) const
	{
		return (getNodeAt(index)->data);
	}

	/**
	* @brief Getter of the data held by the first Node
	* @return Data held by the first Node
	*/
	T &first()
	{
		return _head->data;
	}

	/**
	* @brief Constant getter of the data held by the first Node
	* @return Data held by the first Node
	*/
	T &cfirst() const
	{
		return _head->data;
	}

	/**
	* @brief Getter of the data held by the last Node
	* @return Data held by the last Node
	*/
	T &last()
	{
		return _tail->data;
	}

	/**
	* @brief Constant getter of the data held by the last Node
	* @return Data held by the last Node
	*/
	T &clast() const
	{
		return _tail->data;
	}

	/**
	* @brief Getter of a sublist which's ends are described by the index_1 and index_2 parameters
	* @param index_1 Position in the main linked list marking the first position of the sublist
	* @param index_2 Position in the main linked list marking the last position of the sublist
	* @return The sublist requested
	*/
	LinkedList &subList(const unsigned int &index_1 = 0, const unsigned int &index_2 = 0)
	{
		LinkedList<T> *subList = new LinkedList<T>;
		for (unsigned int i = index_1; i <= index_2; ++i)
			subList->append(cat(i));
		return *subList;
	}

	/**
	* @brief Getter of the size of the linked list, corresponding to the number of the Nodes
	* @return The number of nodes in the linked list
	*/
	unsigned int size() const
	{
		return _size;
	}

	/**
	* @brief Getter of the number of Node holding the same data value described by the data parameter
	* @param data  Data held by the Nodes to count
	* @return The number of Nodes holding the same data value described by the data parameter
	*/
	unsigned int count(const T &data) const
	{
		unsigned int count = 0;
		for (unsigned int i = 0; i < _size; ++i)
			if (cat(i) == data)
				count++;
		return count;
	}

	/**
	* @brief Checks if the linked list contains a Node with the data value described by the data parameter
	* @param data Data value to check
	* @return True or false depending on if there's a Node holding the data value described by the data parameter
	*/
	bool contains(const T &data) const
	{
		for (unsigned int i = 0; i < _size; ++i)
			if (cat(i) == data)
				return true;
		return false;
	}

	/**
	* @brief Checks if the first Node in the linked list contains the data value described by the data parameter
	* @param data Data value to check
	* @return True or false depending on if the first Node holds the data value described by the data parameter
	*/
	bool startsWith(const T &data) const
	{
		return (_head->data == data);
	}

	/**
	* @brief Checks if the last Node in the linked list contains the data value described by the data parameter
	* @param data Data value to check
	* @return True or false depending on if the last Node holds the data value described by the data parameter
	*/
	bool endsWtih(const T &data) const
	{
		return (_tail->data == data);
	}

	/**
	* @brief Checks if the linked list is empty
	* @return True if the linked list doesn't contain any Nodes, false if it does
	*/
	bool empty() const
	{
		if (!_size)
			return true;
		else
			return false;
	}

	/**
	* @brief Converts the linked list to a standard std::list
	* @return The standard C list equivalent of the linked list
  	*/
	std::list<T>& toStdList() const
	{
		std::list<T> list = new std::list<T>;
		for (unsigned int i = 0; i < _size; ++i)
			list.push_back(cat(i));
		return list;
	}

	/**
	* @brief Destructor of the linked list
	*/
	~LinkedList()
	{
		clear();
	}

	/**
	* @brief Operator [] that gives access to the data held by the Node corresponding to the position requested requested by the index parameter
	* @param index Position of the requested Node
	* @return The data held by the requested Node
	*/
	T &operator[](const int &index) const
	{
		return cat(index);
	}

	/**
	* @brief Operator += overloaded with a data
	* @param data Data held by a new Node to attach at the end of the linked list
	* @return The newly formed linked list
	*/
	LinkedList &operator+=(const T &data) const
	{
		this->append(data);
		return *this;
	}

	/**
	* @brief Operator += overloaded with a linked list
	* @param linkedList Linked list to attach at the end of the main linked list
	* @return The newly formed linked list
	*/
	LinkedList &operator+=(const LinkedList &linkedList) const
	{
		this->appendList(linkedList);
		return *this;
	}

	/**
	* @brief Operator + that combines two linked lists into one
	* @param linkedList Linked list to attach at the end of the main linked list
	* @return The newly formed linked list
	*/
	LinkedList &operator+(const LinkedList &linkedList) const
	{
		LinkedList<T> *linkedListReturn = new LinkedList<T>;
		linkedListReturn = this;
		linkedListReturn->appendList(linkedList);
		return *linkedListReturn;
	}

	/**
	* @brief Operator << overloaded with a data
	* @param data Data held by the new Node added at the end
	* @return The newly formed linked list
	*/
	LinkedList &operator<<(const T &data) const
	{
		this->append(data);
		return *this;
	}

	/**
	* @brief Operator << overloaded with a linked list
	* @param linkedList Linked list to attach at the end of the main linked list
	* @return The newly formed linked list
	*/
	LinkedList &operator<<(const LinkedList &linkedList) const
	{
		this->appendList(linkedList);
		return *this;
	}

	/**
	* @brief Compares the number of Nodes of two linked lists
	* @param linkedList Linked list to compare the number of Nodes to
	* @return True or false depending if the number of Nodes in the main linked list is smaller to the number of Nodes in the linked list in parameters
	*/
	bool operator<(const LinkedList &linkedList) const
	{
		return (_size < linkedList.size());
	}

	/**
	* @brief Compares the number of Nodes of two linked lists
	* @param linkedList Linked list to compare the number of Nodes to
	* @return True or false depending if the number of Nodes in the main linked list is greater to the number of Nodes in the linked list in parameters
	*/
	bool operator>(const LinkedList &linkedList) const
	{
		return(_size > linkedList.size()); eturn false;
	}

	/**
	* @brief Compares the number of Nodes of two linked lists
	* @param linkedList Linked list to compare the number of Nodes to
	* @return True or false depending if the number of Nodes in the main linked list is smaller or equal to the number of Nodes in the linked list in parameters
	*/
	bool operator<=(const LinkedList &linkedList) const
	{
		return (_size <= linkedList.size());
	}

	/**
	* @brief Compares the number of Nodes of two linked lists
	* @param linkedList Linked list to compare the number of Nodes to
	* @return True or false depending if the number of Nodes in the main linked list is greater or equal to the number of Nodes in the linked list in parameters
	*/
	bool operator>=(const LinkedList &linkedList) const
	{
		return (_size >= linkedList.size());
	}
};

//#endif
